#include "mem.h"
#include "mem_internals.h"
#include <assert.h>
#include <stdio.h>

#define TEST_SIZE 8192
#define TEST_BLOCK_SIZE 200
#define ZERO_SIZE 0

static void test_heap() {
    struct region *heap = heap_init(TEST_SIZE);
    assert(heap && "Test 1 failed");
    heap_term();
    printf("Test 1 passed\n");
}

static void text_expansion() {
    struct region *heap = heap_init(ZERO_SIZE);
    assert(heap && "Test 2 failed");
    _malloc(TEST_BLOCK_SIZE);
    assert(heap->size == TEST_BLOCK_SIZE);
    printf("Test 2 passed\n");
}

static void test_expansion_moved() {
    void *heap = heap_init(ZERO_SIZE);
    void *lock = heap + TEST_SIZE;
    void *block1 = _malloc(TEST_SIZE);
    void *block2 = _malloc(TEST_SIZE);
    assert(block1 && "Test 3 failed");
    assert(block2 && "Test 3 failed");
    _free(block1);
    _free(block2);
    _free(lock);
    assert(heap && "Test 3 failed");
    printf("Test 3 passed\n");
}

static void test_blocks() {
    heap_init(TEST_SIZE);
    void *block1 = _malloc(TEST_BLOCK_SIZE);
    void *block2 = _malloc(TEST_BLOCK_SIZE);
    void *block3 = _malloc(TEST_BLOCK_SIZE);
    _free(block1);
    assert(block2 && "Test 4 failed");
    assert(block3 && "Test 4 failed");
    printf("Test 4 passed\n");
    _free(block2);
    assert(block3 && "Test 5 failed");
    printf("Test 5 passed\n");
    _free(block3);
}

int main() {
    test_heap();
    text_expansion();
    test_expansion_moved();
    test_blocks();
    return 0;
}
